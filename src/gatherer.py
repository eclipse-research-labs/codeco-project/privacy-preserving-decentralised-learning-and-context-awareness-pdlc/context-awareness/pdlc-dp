# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

from pp_module.ca import CA_operations
from pp_module.rl import RL_operations
from pp_module.gnn import GNN_operations
import threading
from queue import Queue
import time
import os

class Gatherer:
    # Flags to check if the threads are ready to collect information
    CA_ready_flag = True
    RL_ready_flag = True
    GNN_ready_flag = True

    # Lists to store the results used by CA, RL and GNN
    CA_queue = Queue()
    RL_queue = Queue()
    GNN_queue = Queue()

    # Amount of time to wait before starting a new thread
    CA_wait_time = int(os.getenv('GNN_WAIT_TIME', '10'))
    RL_wait_time = int(os.getenv('GNN_WAIT_TIME', '10'))
    GNN_WAIT_TIME = int(os.getenv('GNN_WAIT_TIME', '10'))

    # Start the threads
    def start_threads():
        # Start a CA thread
        threading.Thread(target=Gatherer.use_CA).start()

        # Start an RL thread
        threading.Thread(target=Gatherer.use_RL).start()

        # Start a GNN thread
        threading.Thread(target=Gatherer.use_GNN).start()

    # Start a CA thread and when it finishes, start another one
    def use_CA():
        CA_start_time = time.time()
        N = Gatherer.CA_queue.qsize()

        CA_list = []
        for i in range(N):
            CA_list.append(Gatherer.CA_queue.get())

        Gatherer.CA_ready_flag = False
        CA_operations(CA_list)
        Gatherer.CA_ready_flag = True

        CA_end_time = time.time()
        CA_time = CA_end_time - CA_start_time

        # If the time is less than the wait time, sleep for the difference
        if CA_time < Gatherer.CA_wait_time:
            time.sleep(Gatherer.CA_wait_time - CA_time)

        # Start a new use_CA thread
        threading.Thread(target=Gatherer.use_CA).start()
        return
    
    # Start an RL thread and when it finishes, start another one
    def use_RL():
        RL_start_time = time.time()
        N = Gatherer.RL_queue.qsize()

        RL_list = []
        for i in range(N):
            RL_list.append(Gatherer.RL_queue.get())

        Gatherer.RL_ready_flag = False
        RL_operations(RL_list)
        Gatherer.RL_ready_flag = True

        RL_end_time = time.time()
        RL_time = RL_end_time - RL_start_time

        # If the time is less than the wait time, sleep for the difference
        if RL_time < Gatherer.RL_wait_time:
            time.sleep(Gatherer.RL_wait_time - RL_time)

        # Start a new use_RL thread
        threading.Thread(target=Gatherer.use_RL).start()
        return
    
    # Start a GNN thread and when it finishes, start another one
    def use_GNN():
        GNN_start_time = time.time()
        N = Gatherer.GNN_queue.qsize()

        GNN_list = []
        for i in range(N):
            GNN_list.append(Gatherer.GNN_queue.get())

        Gatherer.GNN_ready_flag = False
        GNN_operations(GNN_list)
        Gatherer.GNN_ready_flag = True

        GNN_end_time = time.time()
        GNN_time = GNN_end_time - GNN_start_time

        # If the time is less than the wait time, sleep for the difference
        if GNN_time < Gatherer.GNN_WAIT_TIME:
            time.sleep(Gatherer.GNN_WAIT_TIME - GNN_time)

        # Start a new use_GNN thread
        threading.Thread(target=Gatherer.use_GNN).start()
        return
