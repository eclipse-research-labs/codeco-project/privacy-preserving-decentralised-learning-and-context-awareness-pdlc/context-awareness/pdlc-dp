# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Efterpi Paraskevoulakou (UPRC) - Author

# import libraries
import csv, os, json, time, sys,re
from pp_module.features_dict import rl_dict
import pandas as pd 
import numpy as np
import math


# env variables 
VOLUME_PATH = os.getenv("VOLUME_PATH", "./data")
CA_DIR = os.getenv("CA_DIR",f"{VOLUME_PATH}/CA/data_CA.csv")
GNNS_DATA_CSV = os.getenv("GNN_DIR", f"{VOLUME_PATH}/GNN/data_GNN.csv")
MIN_MAX_DATA_JSON = os.getenv("MIN_MAX_DATA_JSON",f"{VOLUME_PATH}/min_max/min_max.json")
LIST_NODES = os.getenv("LIST_NODES", "node-a,node-b")
# mixed variables 
RL_TEST_INTEGRACIO_CSV = f"{VOLUME_PATH}/RL/test_integracio.csv"
RL_TEST_NODES_CSV = f"{VOLUME_PATH}/RL/test_nodes.csv"
MIN_MAX_DATA_JSON_CA = f"{VOLUME_PATH}/CA/min_max/min_max_ca.json"


# additional variables, lists, dictionaries to implement the enitre logic
nodes_key_exclude = ['pods', 'links']
mdm_feature = ['freshness', "portability", "compliance"]
data_paths = [f"{VOLUME_PATH}/RL/", f"{VOLUME_PATH}/CA/", f"{VOLUME_PATH}/GNN/", f"{VOLUME_PATH}/min_max/"]
cols_to_convert = ["node_cpu", "node_mem"]
ca_features_norm = ['node_cpu_usage', 'node_memory_values', 'node_energy_values', 'node_ibw', 'node_ebw']
ca_normalized_values_dict = {"node_cpu_usage": "normalized_cpu_values", "node_memory_values": "nomalized_memory_values", "node_energy_values": "nomalized_energy_values", 
                                "node_ibw": "normalized_node_ibw", "node_ebw": "normalized_node_ebw", "node_degree": "node_degree", "node_net_failure": "node_net_failure", "node_failure": "node_failure", "node_net_energy":"node_net_energy", "freshness": "freshness", "portability": "portability", "compliance":"compliance"}

ca_order = ['timestamp',"node_name","node_cpu_usage","normalized_cpu_values" ,"node_memory_values","nomalized_memory_values","node_energy_values", "nomalized_energy_values","node_ibw","normalized_node_ibw", "node_ebw", "normalized_node_ebw", "node_degree", "node_net_failure", "node_failure", "node_net_energy", "freshness", "portability", "compliance"]




def create_directory_if_not_exists(directory_path:str):
    """
    ### Description Create a directory if it does not already exist.

    Args:
        - directory_path (str): The path to the directory to be created.

    Returns:
        - None, just creates the directories
    """
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
        print(f"Directory '{directory_path}' created successfully.")
        result  = 0  
    else:
        print(f"Directory '{directory_path}' already exists.")
        result = 1
    
    return result



def create_csv_headers(filename_:str, features: list):
    """
    ### Create CSV file headers if the file does not already exist.

    Args:
        - filename_ (str): The name of the CSV file including the entire path to it.
        - features (list): A list of header column names based on subcomponet's csv file (different 
           for RL, CA, and GNNs).

    Returns:
         - None, only writes the header to the file. This function helps defining the 
           structure of the csv files that will be consumed by the other PDLC's sub-components
    """
    if not os.path.isfile(filename_):
        with open(f'{filename_}', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            # Write header
            writer.writerow(features)
    else:
        pass

def initial_state():
    """
    ### Function supporting the initial state dictionaries to be stored in the csv files.

    Args:
        - None, just the initialization

    Returns:

        -tuple: A tuple containing dictionaries for CA node structure, RL test integracio, 
        RL test nodes, and GNN data csv files.
    """
    ca_node_structure = {
        "timestamp": None,
        "node_name": None,
        "node_cpu_usage": None,
        "node_memory_values": None,
        "node_energy_values": None,
        "node_sec": None,
        "node_failure": None,
        "portability": None,
        "freshness": None,
        "compliance": None,
        "link_id": None,
        "link_failure": None,
        "node_net_failure": None,
        "node_ibw": None,
        "node_ebw": None,
        "node_latency": None,
        "path_length": None,
        "link_energy": None,
        "node_net_energy": None,
        "packet_loss": None
    }

    rl_test_integracio = {
        "timestamp": None,
        "Podname": None,
        "appName":None,
        "service_name":None,
        "cpu": None,
        "ram": None,
        "real_cpu": None,
        "real_ram": None,
        "namespace": None
    }

    rl_test_nodes = {
        "timestamp": None,
        "nodeid": None,
        "cpu_available": None,
        "ram_available": None
    }

    gnn_data = {
        "timestamp": None,
        "node_name": None,
        "normalized_cpu_values": None,
        "normalized_memory_values": None
    }

    return ca_node_structure, rl_test_integracio, rl_test_nodes, gnn_data


 

def gather_pod_info(pods:list, timestamp):
    """
    ### Gather information about Pods.
    
    Description: This function is mainly used for the RL sub-component in order to gather
                cpu and memory per pod and their fixed values
    Args:
        pods (list): List of Pods.
        timestamp (str): Timestamp for the data.

    Returns:
        list: List of dictionaries containing information about Pods.
    """
    order = ["timestamp","Podname","appName","service_name","cpu","ram","real_cpu","real_ram","namespace"]
    pod_row, pod_list = {},[]
    for pod in pods:
        for pod_key, pod_value in pod.items():
            
            for rl_k,rl_v in rl_dict.items():
                if pod_key == rl_v:
                    pod_row.update({rl_k:pod_value})
        pod_row.update({"timestamp":timestamp})
        pod_row = {key: pod_row[key] for key in order if key in pod_row}
        pod_list.append(pod_row)
        pod_row={}
    
    return pod_list


def append_ordered_dict_to_csv_file(ordered_dict:dict, csv_file:str):


    """
    ### Append an ordered dictionary as a row in a CSV file.

    Args:
        - ordered_dict (OrderedDict): The ordered dictionary to append.
        - csv_file (str): The path to the CSV file.

    Returns:
        - None
    """
    with open(csv_file, 'a', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=ordered_dict.keys())
        
        # Write header if file is empty
        if file.tell() == 0:
            writer.writeheader()
        
        # Write data
        writer.writerow(ordered_dict)


        
         
def filter_df_for_node(df:pd.DataFrame, node_name:str, type:str):
    """
    ### Filter a DataFrame to extract CPU and memory values filtered by a specific node.

    Args:
        - df (pandas.DataFrame): The DataFrame containing CPU and memory values.
        - node_name (str): The name of the node to filter.

    Returns:
        - pandas.DataFrame or None: A DataFrame containing CPU and memory values for the specified node,
          or None if no data is found for the node.
    """
    if type == "gnns":
        filtered_df = df[df['node_name'] == node_name][['timestamp','node_name','node_cpu', 'node_mem']]
        if not filtered_df.empty:
            return filtered_df
        else:
            return None
    else:
        filtered_df = df[df['node_name'] == node_name][['timestamp',"node_cpu_usage", "node_memory_values","node_energy_values", "node_ibw", "node_ebw", "node_degree", "node_net_failure", "node_failure", "node_net_energy", 'freshness', "portability", "compliance"]]
        if not filtered_df.empty:
            return filtered_df
        else:
            return None



def normalize_column(column:list, min_value, max_value):

    if min_value == max_value:
        # If all numbers are the same, we can normalize to 0.5 for example
        return [0 for _ in column]

    normalized_numbers = [(x - min_value) / (max_value - min_value) for x in column]
    return normalized_numbers



def min_max_from_json(node_name:str, metric_type:str, min_max_value:float):
    """
    ### Update the minimum or maximum value for a specified metric type associated with a node in a JSON file.

    Args:
        - node_name (str): The name of the node.
        - metric_type (str): The type of metric (e.g., 'min_cpu', 'max_memory').
        - min_max_value (float): The new minimum or maximum value to set.

    Returns:
        - float or None: The updated value of the specified metric type for the node if updated successfully, 
          otherwise the existing value or None if the node is not found or an error occurs.
    """
    try:
        # Load JSON content from file
        with open(MIN_MAX_DATA_JSON, 'r') as json_file:
            json_content = json.load(json_file)
        
        # Iterate through nodes in JSON content
        for node_element in json_content:
            if node_element['node_name'] == node_name:
                existing_value = node_element.get(metric_type, None)  # Get existing value, if any
                
                # Update minimum value if metric type starts with 'min'
                if metric_type.startswith("min"):
                    if existing_value is None or existing_value > min_max_value:
                        node_element[metric_type] = min_max_value  # Update value
                        with open(MIN_MAX_DATA_JSON, 'w') as file:
                            json.dump(json_content, file)  # Write updated JSON content to file
                        return node_element[metric_type]
                    else:
                        return existing_value
                
                # Update maximum value if metric type starts with 'max'
                else:
                    if existing_value is None or existing_value < min_max_value:
                        node_element[metric_type] = min_max_value  # Update value
                        with open(MIN_MAX_DATA_JSON, 'w') as file:
                            json.dump(json_content, file)  # Write updated JSON content to file
                        return node_element[metric_type]
                    else:
                        return existing_value
                
                break  # Exit loop after finding the node
        
        # Raise exception if node is not found
        else:
            raise ValueError(f"Node with name '{node_name}' not found in JSON content.")
    
    # Handle exceptions and return None
    except Exception as e:
        print(f"An error occurred: {e}")
        return None

def convert_str_to_float(metric_str):
    # Use a regular expression to extract the numeric part
    match = re.match(r'(\d+(\.\d+)?)', metric_str)
    if match:
        return float(match.group(1))
    else:
        raise ValueError("Invalid memory format")


def store_final_csv(dfs:list, type:str):
    """
    ## Description:
    This function stores the final dataframs to csv files

    Args:
        - dfs (list): List including the dataframes 
        - type (str): either "gnns" or "ca" 

    Returns:
        None (just appends new values to the csv files)
    """

    all_values_df = pd.concat(dfs, ignore_index=False)
    all_values_df = all_values_df.sort_index()
    if type =="gnns": 
        gnns_final_df = all_values_df[['timestamp', 'node_name', 'normalized_cpu_values', 'normalized_memory_values', 'min_cpu', 'max_cpu', 'min_memory', 'max_memory']]
        if not os.path.exists(GNNS_DATA_CSV):
            gnns_final_df.set_index('timestamp', inplace=True)
            gnns_final_df.to_csv(GNNS_DATA_CSV, index='timestamp')
        else:
            final_gnns_rows = gnns_final_df.to_dict(orient='records')
            for new_row in final_gnns_rows:
                append_ordered_dict_to_csv_file(new_row, GNNS_DATA_CSV)
    else:


        ca_final_df = all_values_df[['timestamp',"node_name", "node_cpu_usage","normalized_cpu_values", "node_memory_values","nomalized_memory_values","node_energy_values", "nomalized_energy_values","node_ibw","normalized_node_ibw", "node_ebw", "normalized_node_ebw", "node_degree", "node_net_failure", "node_failure", "node_net_energy", 'freshness', "portability", "compliance"]]
        

        if not os.path.exists(CA_DIR):
            ca_final_df.set_index('timestamp', inplace=True)
            ca_final_df.to_csv(CA_DIR, index='timestamp')
        else:
            final_ca_rows = ca_final_df.to_dict(orient='records')
            for new_row in final_ca_rows:
                append_ordered_dict_to_csv_file(new_row, CA_DIR)


def retrieve_values_mdm(metrics_list:list, feature:str):

    """
    ## Description:
    This function is responsible for gathering the pod-level MDM metrics

    ### Args:
        - metrics_list (List): list with mdm metrics 
        - feature (str): the name of each of the MDM metrics

    Returns:
        - feature_value_list (List): The specific MDM feature metrics as a list (pod-level metrics)
    """

    feature_value_list = []

    for element in metrics_list:
        try:
            feature_value_list.append(element.get(feature))
        except Exception as e:
            print(e)
            feature_value_list.append("nan")

    return feature_value_list


def retrieve_netma_values(links_list: list, feature: str):

    """
    ## Description:
    This function is responsible for gathering the pod-level NetMA metrics

    ### Args:
        - links_list (List): list with NetMA metrics (with regards feature: "Links").
        - feature (str): the name of each of the NetMA metrics

    Returns:
        - feature_value_list (List): The specific NetMA feature metrics as a list  
    """

    feature_value_list = []

    if feature == "latency":
        for link in links_list:
            feature_value_list.append(link.get("latency", "nan"))
    elif feature == "link_id":
        for link in links_list:
            feature_value_list.append(link.get("link_id", "nan"))
    elif feature == "link_failure":
        for link in links_list:
            feature_value_list.append(link.get("link_failure", "nan"))
    elif feature == "link_energy":
        for link in links_list:
            feature_value_list.append(link.get("link_energy", "nan"))
    elif feature == "path_length":
        for link in links_list:
            feature_value_list.append(link.get("path_length", "nan"))
    elif feature == "node_net_energy":
        for link in links_list:
            feature_value_list.append(link.get("node_net_energy", "nan"))
    elif feature == "node_net_failure":
        for link in links_list:
            feature_value_list.append(link.get("node_net_failure", "nan"))
    elif feature == "packetloss":
        for link in links_list:
            feature_value_list.append(link.get("packet_loss", "nan")),

    feature_value_list = [x for x in feature_value_list if not "nan"]

    return feature_value_list    
