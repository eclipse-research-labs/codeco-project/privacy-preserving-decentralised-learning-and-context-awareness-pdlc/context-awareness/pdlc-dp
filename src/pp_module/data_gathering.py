# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Efterpi Paraskevoulakou (UPRC) - Author

# import some necessary libraries
from datetime import datetime
import random
import time, os
import numpy as np



def generate_timestamp():
    '''
    ## Description:
    Function that generates faketimestamps (not every 1 min.)

    ### Args:
        None
    
    ### Returns:
        fake_timestamp(pd.Datetime): The fake timestamp
    '''
    # Get the current Unix timestamp
    current_timestamp = time.time()

    # Convert the Unix timestamp to a datetime object
    datetime_object = datetime.fromtimestamp(current_timestamp)

    # Format the datetime object as a string using strftime
    fake_timestamp = datetime_object.strftime('%Y-%m-%d %H:%M:%S')

    return fake_timestamp


def gather_data(i):
    mean = 50
    std_dev = 15

    # Generate 1000 samples from the normal distribution
    samples = np.random.normal(loc=mean, scale=std_dev, size=100)

    # Clip the values to be within [0, 100]
    samples = np.clip(samples, 0, 100)
    '''
    ## Description:
    Placeholder function to generate fake data

    ### Args:
        None
    
    ### Returns:
        cluster_batch (dict): The batch of data 
    '''
    # Placeholder function for generate_timestamp()

    
    cluster_batch =[
            {
                "node1": {
                    "node_name": "node1",
                    "type": "C",
                    "links": [
                        {
                            "type": "N",
                            "node_name": "network-node",
                            "link_id": "link-node1-network-node",
                            "link_type": "P"
                        },
                        {
                            "type": "C",
                            "node_name": "node2",
                            "link_id": "link-node1-node2",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        },
                        {
                            "type": "C",
                            "node_name": "node3",
                            "link_id": "link-node1-node3",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        }
                    ],
                    "pods": [],
                    "ebw": f"{random.randint(1,10)}G",
                    "node_degree": 1,
                    "node_throughput": f"{random.randint(1,10)}G",
                    "ibw": "2G",
                    "node_cpu": f"{random.choice(samples)}",
                    "node_mem": f"{random.choice(samples)}Gi",
                    "node_failure": "1",
                    "node_energy": "3",
                    "node_sec": "0"
                },
                "node2": {
                    "node_name": "node2",
                    "type": "C",
                    "links": [
                        {
                            "type": "N",
                            "node_name": "network-node",
                            "link_id": "link-node2-network-node",
                            "link_type": "P"
                        },
                        {
                            "type": "C",
                            "node_name": "node1",
                            "link_id": "link-node2-node1",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        },
                        {
                            "type": "C",
                            "node_name": "node3",
                            "link_id": "link-node2-node3",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        }
                    ],
                    "pods": [],
                    "ebw": f"{random.randint(1,10)}G",
                    "node_degree": 2,
                    "node_throughput": "1G",
                    "ibw": f"{random.randint(1,10)}G",
                    "node_cpu": f"{random.choice(samples)}",
                    "node_mem": f"{random.choice(samples)}Gi",
                    "node_failure": "1",
                    "node_energy": "3",
                    "node_sec": "0"
                },
                "node3": {
                    "node_name": "node3",
                    "type": "C",
                    "links": [
                        {
                            "type": "N",
                            "node_name": "network-node",
                            "link_id": "link-node3-network-node",
                            "link_type": "P"
                        },
                        {
                            "type": "C",
                            "node_name": "node1",
                            "link_id": "link-node3-node1",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        },
                        {
                            "type": "C",
                            "node_name": "node2",
                            "link_id": "link-node3-node2",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        }
                    ],
                    "pods": [
                        {
                            "appName": "acm-swm-app",
                            "pod_name": "test-pod-1",
                            "pod_cpu": f"{random.choice(samples)}",
                            "pod_mem": f"{random.choice(samples)}Gi",
                            "pod_failure": "1",
                            "pod_energy": "3",
                            "pod_sec": "0",
                            "service_name": "front-end",
                            "max_pod_cpu": "2",
                            "max_pod_mem": "4Gi",
                            "cluster": "test-cluster",
                            "namespace": "he-codeco-acm",
                            "freshness": "stale",
                            "compliance": "non-compliant",
                            "portability": "low"
                        },
                        {
                            "appName": "acm-swm-app",
                            "pod_name": "test-pod-2",
                            "pod_cpu": f"{random.choice(samples)}",
                            "pod_mem": f"{random.choice(samples)}Gi",
                            "pod_failure": "1",
                            "pod_energy": "3",
                            "pod_sec": "2",
                            "service_name": "backend",
                            "max_pod_cpu": "2",
                            "max_pod_mem": "4Gi",
                            "cluster": "test-cluster",
                            "namespace": "he-codeco-acm",
                            "freshness": "stale",
                            "compliance": "unknown",
                            "portability": "unknown"
                        }
                    ],
                    "ebw": f"{random.randint(1,10)}G",
                    "node_degree": "3-node_degree",
                    "node_throughput": "2G",
                    "ibw": f"{random.randint(1,10)}G",
                    "node_cpu": f"{random.choice(samples)}",
                    "node_mem": f"{random.choice(samples)}Gi",
                    "node_failure": "1-node_failure",
                    "node_energy": "3-node_energy",
                    "node_sec": "0"
                },
                "network-node": {
                    "node_name": "network-node",
                    "type": "N",
                    "links": [
                        {
                            "type": "C",
                            "node_name": "node1",
                            "link_id": "link-network-node-node1",
                            "link_type": "P"
                        },
                        {
                            "type": "C",
                            "node_name": "node2",
                            "link_id": "link-network-node-node2",
                            "link_type": "P"
                        },
                        {
                            "type": "C",
                            "node_name": "node3",
                            "link_id": "link-network-node-node3",
                            "link_type": "P"
                        }
                    ],
                    "pods": [],
                    "node_net_failure": 0
                },
                "timestamp": f"2024-05-29 02:{i:02}:21"
            },
            {
                "node1": {
                    "node_name": "node1",
                    "type": "C",
                    "links": [
                        {
                            "type": "N",
                            "node_name": "network-node",
                            "link_id": "link-node1-network-node",
                            "link_type": "P"
                        },
                        {
                            "type": "C",
                            "node_name": "node2",
                            "link_id": "link-node1-node2",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        },
                        {
                            "type": "C",
                            "node_name": "node3",
                            "link_id": "link-node1-node3",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        }
                    ],
                    "pods": [],
                    "ebw": "1G",
                    "node_degree": 1,
                    "node_throughput": "0G",
                    "ibw": "2G",
                    "node_cpu": f"{random.choice(samples)}",
                    "node_mem": f"{random.choice(samples)}Gi",
                    "node_failure": "1",
                    "node_energy": "3",
                    "node_sec": "0"
                },
                "node2": {
                    "node_name": "node2",
                    "type": "C",
                    "links": [
                        {
                            "type": "N",
                            "node_name": "network-node",
                            "link_id": "link-node2-network-node",
                            "link_type": "P"
                        },
                        {
                            "type": "C",
                            "node_name": "node1",
                            "link_id": "link-node2-node1",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        },
                        {
                            "type": "C",
                            "node_name": "node3",
                            "link_id": "link-node2-node3",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        }
                    ],
                    "pods": [
                        {
                            "appName": "acm-swm-app",
                            "pod_name": "test-pod-1",
                            "pod_cpu": f"{random.choice(samples)}",
                            "pod_mem": f"{random.choice(samples)}Gi",
                            "pod_failure": "1",
                            "pod_energy": "3",
                            "pod_sec": "0",
                            "service_name": "front-end",
                            "max_pod_cpu": "2",
                            "max_pod_mem": "4Gi",
                            "cluster": "test-cluster",
                            "namespace": "he-codeco-acm",
                            "freshness": "stale",
                            "compliance": "non-compliant",
                            "portability": "low"
                        }
                    ],
                    "ebw": "2G",
                    "node_degree": 2,
                    "node_throughput": "1G",
                    "ibw": "2G",
                    "node_cpu": f"{random.choice(samples)}",
                    "node_mem": f"{random.choice(samples)}Gi",
                    "node_failure": "1",
                    "node_energy": "3",
                    "node_sec": "0"
                },
                "node3": {
                    "node_name": "node3",
                    "type": "C",
                    "links": [
                        {
                            "type": "N",
                            "node_name": "network-node",
                            "link_id": "link-node3-network-node",
                            "link_type": "P"
                        },
                        {
                            "type": "C",
                            "node_name": "node1",
                            "link_id": "link-node3-node1",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        },
                        {
                            "type": "C",
                            "node_name": "node2",
                            "link_id": "link-node3-node2",
                            "link_type": "L",
                            "ibw": "1G",
                            "latency": "2e6",
                            "packet_loss": 0,
                            "path_failure": 0,
                            "path_length": 1
                        }
                    ],
                    "pods": [
                        {
                            "appName": "acm-swm-app",
                            "pod_name": "test-pod-2",
                            "pod_cpu": f"{random.choice(samples)}",
                            "pod_mem": f"{random.choice(samples)}Gi",
                            "pod_failure": "1",
                            "pod_energy": "3",
                            "pod_sec": "2",
                            "service_name": "backend",
                            "max_pod_cpu": "2",
                            "max_pod_mem": "4Gi",
                            "cluster": "test-cluster",
                            "namespace": "he-codeco-acm",
                            "freshness": "stale",
                            "compliance": "unknown",
                            "portability": "unknown"
                        }
                    ],
                    "ebw": "3G",
                    "node_degree": 3,
                    "node_throughput": "2G",
                    "ibw": "2G",
                    "node_cpu": f"{random.choice(samples)}",
                    "node_mem": f"{random.choice(samples)}Gi",
                    "node_failure": "1",
                    "node_energy": "3",
                    "node_sec": "0"
                },
                "network-node": {
                    "node_name": "network-node",
                    "type": "N",
                    "links": [
                        {
                            "type": "C",
                            "node_name": "node1",
                            "link_id": "link-network-node-node1",
                            "link_type": "P"
                        },
                        {
                            "type": "C",
                            "node_name": "node2",
                            "link_id": "link-network-node-node2",
                            "link_type": "P"
                        },
                        {
                            "type": "C",
                            "node_name": "node3",
                            "link_id": "link-network-node-node3",
                            "link_type": "P"
                        }
                    ],
                    "pods": [],
                    "node_net_failure": 0
                },
                "timestamp": f"2024-05-29 02:{i+1:02}:21"
            }
        ]

    return cluster_batch
