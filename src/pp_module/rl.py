# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Efterpi Paraskevoulakou (UPRC) - Author

# import necessary libraries
from datetime import datetime
import sys, time
from pp_module.utilities import RL_TEST_INTEGRACIO_CSV, RL_TEST_NODES_CSV, nodes_key_exclude, data_paths
from pp_module.utilities import append_ordered_dict_to_csv_file, gather_pod_info, create_directory_if_not_exists, initial_state, create_csv_headers, convert_str_to_float
from pp_module.features_dict import rl_dict
from pp_module.data_gathering import gather_data


def RL_operations(cluster_instances):
    """
    ### Perform operations in order to create the rl csv files used by the RL
        subcomponent

    Args:
        - cluster_instances (list): A list of cluster instances.

    Returns:
        - None,  only saves the data to the test_nodes.csv and test_integracio.csv 

    """
    result = create_directory_if_not_exists(data_paths[0]) #fiirstly checks if the proper directory for sorting the data exists (also a debugging method
    if result == 0:
            ca_node_structure, rl_test_integracio, rl_test_nodes, gnn_data= initial_state()
            rl_test_integracio_features = [v for v in list(rl_test_integracio.keys())]
            rl_test_nodes_features = [v for v in list(rl_test_nodes.keys())]

            create_csv_headers(RL_TEST_INTEGRACIO_CSV, rl_test_integracio_features) # create the test_integracio file for the rl component
            create_csv_headers(RL_TEST_NODES_CSV, rl_test_nodes_features) # create the test_nodes file for the rl component
    else:
        pass
    
    if len(cluster_instances) == 0:
        print("[RLs INFO]- here an empty list occurs")
        return None
    
    print("[RL - INFO]- data exist the collection has begun")
    for cluster_instance in cluster_instances:

        node_row, node_info = {}, []
        timestamp = cluster_instance['timestamp']
        for element in cluster_instance:
            if element == "timestamp":
                pass
            else:
                node_element = cluster_instance[element]
                if node_element['type'] == "C":
                    
                    pods = node_element['pods']
                    pod_list = gather_pod_info(pods, timestamp)

                    
                    for element in pod_list:
                        element['cpu'] = convert_str_to_float(element['cpu'])
                        element['ram'] = convert_str_to_float(element['ram'])
                        element['real_cpu'] = convert_str_to_float(element['real_cpu'])
                        element['real_ram'] = convert_str_to_float(element['real_ram'])
                        element["namespace"] = element["namespace"]
                        append_ordered_dict_to_csv_file(element, RL_TEST_INTEGRACIO_CSV)
                    #print(f"pod metrics appended to {RL_TEST_INTEGRACIO_CSV}")
                    


                    node = {key: value for key, value in node_element.items() if key not in nodes_key_exclude}

                    for node_k, node_v in node.items():
                        for rl_key, rl_value in rl_dict.items():
                            if node_k == rl_value:
                                node_row.update({rl_key:node_v})
                                node_row.update({'timestamp':timestamp})


                    node_info.append({"timestamp":node_row["timestamp"], "nodeid":node_row['node_name'], "cpu_available": convert_str_to_float(node_row["cpu_available"]),  "ram_available": convert_str_to_float(node_row["ram_available"])})


            for element_node in node_info:
                append_ordered_dict_to_csv_file(element_node, RL_TEST_NODES_CSV)
            node_info=[]
    
    
    print(f"[INFO] - {datetime.now()}: Files {RL_TEST_INTEGRACIO_CSV} and {RL_TEST_NODES_CSV} updated successfully") 



# To run: `python3 rl.py test`
if len(sys.argv) == 2:
    data_ = None # to be replaced with the gatherer's cluster_batch
    RL_operations(cluster_instances=data_)



'''
experimental purposes

# To run: `python3 rl.py test`
if len(sys.argv) == 2:
    i=0
    while True:
        RL_operations(cluster_instances=gather_data(i))
        time.sleep(2)
        i+=2
'''
