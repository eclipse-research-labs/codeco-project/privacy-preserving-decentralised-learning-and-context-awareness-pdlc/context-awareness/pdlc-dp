# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Efterpi Paraskevoulakou (UPRC) - Author

# import necessary libraries


from datetime import datetime, time
import sys
from pp_module.utilities import  CA_DIR, ca_features_norm, ca_normalized_values_dict, ca_order, data_paths, retrieve_values_mdm
from pp_module.utilities import filter_df_for_node, normalize_column, store_final_csv, create_directory_if_not_exists
import pandas as pd 
from pp_module.data_gathering import gather_data
import time



def CA_operations(cluster_instances):
    """
    ## Description:
    Perform operations in order to create the GNNs csv files used by the CA subcomponent

    Args:
        - cluster_instances (list): A list of cluster instances.

    Returns:
        - None,  only saves the data to the data_CA.csv 

    """
    
    create_directory_if_not_exists(data_paths[1])

    if len(cluster_instances) == 0:
        print("[INFO] - here an empty list occurs")
        return None

    features_ca = ["node_cpu_usage", "node_memory_values","node_energy_values", "node_ibw", "node_ebw"]
    dfs = []
    node_info = []
    for cluster_instance in cluster_instances:
        timestamp = cluster_instance['timestamp']
        for element in cluster_instance:
            node_net_failure_values, node_net_energy_values = [], []
            if element == "timestamp":
                pass
            else:
                node_element = cluster_instance[element]

                if node_element['type'] == "C":
                    node_row = {}
                    pods = node_element['pods']
                    links = node_element['links']
                    for link in links:
                        if link["type"] == "N":
                            network_node = cluster_instance[link['node_name']]
                            network_node_links = network_node['links']
                            for network_node_link in network_node_links:
                                if network_node_link['node_name'] == node_element['node_name']:
                                    node_net_failure_values.append(network_node['node_net_failure'])
                                
                                if network_node_link['type'] == "N":
                                    node_net_energy_values.append(network_node_link['link_energy'])
                                elif network_node_link['type'] == "C":
                                    node_net_energy_values.append(0)
                            
                               
                    
                    node_row.update({'timestamp': timestamp, 
                                    "node_name": node_element['node_name'],
                                    "node_cpu_usage": node_element['node_cpu'],
                                    "node_memory_values": node_element['node_mem'],
                                    "node_energy_values": node_element['node_energy'],
                                    "node_ibw":node_element["ibw"],
                                    "node_ebw":node_element["ebw"],
                                    "node_degree": node_element['node_degree'],
                                    "node_net_failure": sum(node_net_failure_values),
                                    "node_failure": node_element['node_failure'], 
                                    "node_net_energy": sum(node_net_energy_values), 
                                    "freshness": retrieve_values_mdm(pods, "freshness"),
                                    "compliance": retrieve_values_mdm(pods, "compliance"),
                                    "portability": retrieve_values_mdm(pods, "portability")

                                    })

                    node_info.append(node_row)
                else:
                    pass

    ca_df = pd.DataFrame(node_info)


    for feature in features_ca:
        ca_df[feature] = ca_df[feature].str.extract(r'(\d+)').astype(float)

    nodes = ca_df['node_name'].unique()


    for node in nodes:
        norm_feature_values = {}
        df = filter_df_for_node(ca_df, node, "ca")
        node_name = [node for i in range(len(df))]
        for ca_normalization_feature in ca_features_norm:
            if ca_normalization_feature in list(df.columns):
                feature_values = list(df[ca_normalization_feature].values)
                feature_normalization_values = normalize_column(feature_values, min_value=0.000001, max_value=100)
                norm_feature_values.update({ca_normalized_values_dict[ca_normalization_feature]: feature_normalization_values})
                
                
                
        data_df = df.to_dict(orient='list')
        data_df.update({"node_name": node_name})
        data_df.update(norm_feature_values)
        df = pd.DataFrame(data_df)
        df = df[ca_order]
        dfs.append(df)

    store_final_csv(dfs, type="ca")

    print(f"[INFO] - {datetime.now()}: File {CA_DIR} updated successfully")   


# To run: `python3 ca.py test`
if len(sys.argv) == 2:
    data_ = None # to be replaced with the gatherer's cluster_batch
    CA_operations(cluster_instances=data_)



'''
experimental puproses

# To run: `python3 ca.py test`
if len(sys.argv) == 2:
    i=0
    while True:
        CA_operations(cluster_instances=gather_data(i))
        time.sleep(2)
        i+=2
'''

# To run: `python3 ca.py test`