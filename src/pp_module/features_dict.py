# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Efterpi Paraskevoulakou (UPRC) - Author

rl_dict = {"timestamp": "timestamp",
            "node_name": "node_name", 
            "cpu_available":"node_cpu",
            "ram_available":"node_mem",
            "Podname":"pod_name",
            "cpu": "pod_cpu",
            "ram": "pod_mem",
            "real_cpu":"max_pod_cpu",
            "real_ram": "max_pod_mem",
            "appName":"appName", 
            "service_name": "service_name",
            "namespace": "namespace"
            }

gnn = {"timestamp":"timestamp",
        "nodename": "node_name",
        "cpu_values": "node_cpu",
        "memory_values": "node_mem"

}


ca = {"timestamp":"timestamp",
        "node_name": "node_name",
        "node_cpu_usage": "node_cpu",
        "node_memory_values": "node_mem",
        "node_energy_values": "node_energy",
        "node_security":"node_sec",
        "node_failure": "node_failure",
        "portability": "portability",
        "freshness": "freshness",
        "compliance": "compliance",
        "link_id": "link_id",
        "link_failure": "link_failure",
        "node_net_failure": "node_net_failure",
        "node_ibw":"ibw",
        "node_ebw":"ebw",
        "node_latency": "node_latency",
        "uid_visits": "uid_visits",
        "uid_location":"uid_location",
        "node_degree":"node_degree",
        "zone": "zone",
        "path_length": "path_length",
        "link_energy": "link_energy",
        "node_net_energy": "node_net_energy",
        "latency": "latency"

}
