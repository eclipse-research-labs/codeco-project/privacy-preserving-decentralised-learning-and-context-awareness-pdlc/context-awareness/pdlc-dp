# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Efterpi Paraskevoulakou (UPRC) - Author

# import necessary libraries
from datetime import datetime
import os, json, time
import sys
import os.path
from pp_module.utilities import LIST_NODES, cols_to_convert, MIN_MAX_DATA_JSON, data_paths, GNNS_DATA_CSV
from pp_module.utilities import filter_df_for_node, store_final_csv, normalize_column, min_max_from_json, create_directory_if_not_exists
import pandas as pd 
from pp_module.data_gathering import gather_data


def GNN_operations(cluster_instances):
    """
    ## Description:
    Perform operations in order to create the GNNs csv files used by the GNNs subcomponent

    Args:
        - cluster_instances (list): A list of cluster instances.

    Returns:
        - None,  only saves the data to the data_GNN.csv 

    """

    result = create_directory_if_not_exists(data_paths[2]) #fiirstly checks if the proper directory for sotring the data exists (also a debugging method
    result_min_max = create_directory_if_not_exists(data_paths[3])
    features = ['timestamp', 'node_name', 'node_cpu', 'node_mem']
    data_list = []  # List to accumulate data dictionaries
    if len(cluster_instances) == 0:
        print("[GNNs INFO]- here an empty list occurs")
        return None
    
    print("[GNNs - INFO]- data exist the collection has begun")
    for cluster_instance in cluster_instances:
        timestamp = cluster_instance['timestamp']
        for element in cluster_instance:
            if element == "timestamp":
                pass
            else:
                node = cluster_instance[element]

                if node['type'] == "C":
                    
                    node_data = {key: node.get(key) for key in features}
                    node_data.update({'timestamp':timestamp})
                    data_list.append(node_data)
    
    gnn_df = pd.DataFrame(data_list)
    for col in list(gnn_df.columns):
        if col in ['node_cpu', 'node_mem']:
            gnn_df[col] = gnn_df[col].str.extract(r'(\d+)').astype(float)
        else:
            pass
    
    #gnn_df[cols_to_convert] = gnn_df[cols_to_convert].astype(float)

    # if the json is created for the first time
    if not os.path.exists(MIN_MAX_DATA_JSON):
        nodes = gnn_df['node_name'].unique()
        list_of_nodes, dfs = [],[]
        for node in nodes:
            df = filter_df_for_node(gnn_df, node, "gnns")
            # keep the min max values
            min_cpu = df['node_cpu'].min()
            max_cpu = df['node_cpu'].max()
            #normalize the proper column 
            df['normalized_cpu_values'] = normalize_column(df['node_cpu'], min_cpu, max_cpu)
            # the same for the avgNodeMemory
            min_ram = df['node_mem'].min()
            max_ram = df['node_mem'].max()
            df["normalized_memory_values"] = normalize_column(df['node_mem'], min_ram, max_ram)
            df['min_cpu'] = [min_cpu for i in range(len(df))]
            df['max_cpu'] = [max_cpu for i in range(len(df))]
            df['min_memory'] = [min_ram for i in range(len(df))]
            df['max_memory'] = [max_ram for i in range(len(df))]

            

            #create the proper dictionary to fill the json 
            dict_node = {
                "node_name": node,
                "min_cpu": min_cpu,
                "max_cpu": max_cpu,
                "min_ram": min_ram,
                "max_ram": max_ram
            }
            #create the min max values records for gnn denromalization 
            list_of_nodes.append(dict_node)
            dfs.append(df)

        # Store the rrecords to a JSON  file to be used by gnn de-normalization process
        with open(MIN_MAX_DATA_JSON, 'w') as json_file:
            json.dump(list_of_nodes, json_file)
        
        # final operations to gnns.csv
        store_final_csv(dfs, "gnns")

    # if the json file exists
    else:
        nodes = gnn_df['node_name'].unique()
        dfs = []
        for node in nodes:
            df = filter_df_for_node(gnn_df,node, "gnns")
            min_cpu_, max_cpu_ = df['node_cpu'].min(), df['node_cpu'].max()
            min_mem_, max_mem_ = df['node_mem'].min(), df['node_mem'].max()
            min_cpu_updated = min_max_from_json(node, "min_cpu", min_cpu_)
            max_cpu_updated = min_max_from_json(node, "max_cpu", max_cpu_)
            min_mem_updated = min_max_from_json(node, "min_ram", min_mem_)
            max_mem_updated = min_max_from_json(node, "max_ram", max_mem_)


            df['normalized_cpu_values'] = normalize_column(df['node_cpu'], min_cpu_updated, max_cpu_updated)
            df["normalized_memory_values"] = normalize_column(df['node_mem'], min_mem_updated, max_mem_updated)
            df['min_cpu'] = [min_cpu_updated for i in range(len(df))]
            df['max_cpu'] = [max_cpu_updated for i in range(len(df))]
            df['min_memory'] = [min_mem_updated for i in range(len(df))]
            df['max_memory'] = [max_mem_updated for i in range(len(df))]

            dfs.append(df)

        # final operations to gnns.csv
        store_final_csv(dfs, "gnns")

    print(f"[INFO] - {datetime.now()}: File {GNNS_DATA_CSV} updated successfully") 




# To run: `python3 gnn.py test`
if len(sys.argv) == 2:
    data_ = None # to be replaced with the gatherer's cluster_batch
    GNN_operations(cluster_instances=data_)



'''
experimental purposes

# To run: `python3 gnn.py test`
if len(sys.argv) == 2:
    i=0
    while True:
        GNN_operations(cluster_instances=gather_data(i))
        time.sleep(2)
        i+=2
'''
