# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

from gatherer import Gatherer
import time
import os
from k8s_calling.sample import get_sample, codeco_application_deployed
from pp_module.utilities import create_directory_if_not_exists
import shutil # To move files | To be removed when MLOps will be ready

# Get the scrape interval from the environment variables
SCRAPE_INTERVAL = int(os.getenv("SCRAPE_INTERVAL", 1)) # In Seconds

# Path to let the modes for the subcomponents | TO BE REMOVED WHEN MLOps TAKE PLACE
COPY_MODELS_PATH = os.getenv("COPY_MODELS_PATH", "data/")

# Path to the volume
VOLUME_PATH = os.getenv("VOLUME_PATH", "./data")
data_paths = [f"{VOLUME_PATH}/RL/", f"{VOLUME_PATH}/CA/", f"{VOLUME_PATH}/GNN/", f"{VOLUME_PATH}/min_max/"]
# Create the directories if they don't exist
for data_path in data_paths:
    create_directory_if_not_exists(data_path)

""" TO BE REMOVED WHEN MLOps TAKE PLACE"""
def copy_files_and_directories(source_dir, dest_dir):
    # Create destination directory if it doesn't exist
    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)

    # Iterate through all items in the source directory
    for item in os.listdir(source_dir):
        source_item = os.path.join(source_dir, item)
        dest_item = os.path.join(dest_dir, item)
        
        # If it's a file, copy it
        if os.path.isfile(source_item):
            shutil.copy2(source_item, dest_item)
        # If it's a directory, recursively call the function
        elif os.path.isdir(source_item):
            copy_files_and_directories(source_item, dest_item)
copy_files_and_directories("models", COPY_MODELS_PATH)
""" TO BE REMOVED WHEN MLOps TAKE PLACE"""

# Wait until the Codeco application is deployed
while True:
    time.sleep(SCRAPE_INTERVAL)
    if codeco_application_deployed():
        break
Gatherer.start_threads()

while True:
    # Make a Scrape for the whole cluster
    try:
        cluster_sample = get_sample()
    except Exception as err:
        print(f"Unexpected Collection Error (But DP Gathering again in {SCRAPE_INTERVAL} seconds). Error: {err=}, {type(err)=}")
        time.sleep(SCRAPE_INTERVAL)
        continue

    Gatherer.CA_queue.put(cluster_sample)
    Gatherer.RL_queue.put(cluster_sample)
    Gatherer.GNN_queue.put(cluster_sample)

    # Add functionallity to export the cluster topology as well from NETMA CRDs

    time.sleep(SCRAPE_INTERVAL)