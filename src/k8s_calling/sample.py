# Copyright (c) 2024 University of Piraeus Research Centre
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Panagiotis Karamolegkos (UPRC) - Author

import requests
import os, json
from kubernetes import client, config
from datetime import datetime
import random
import time
import yaml

""" MDM Caller """
# Get the URL as an environment variable
MDM_URL = os.getenv('MDM_URL', "http://mdm-controller-service.he-codeco-mdm.svc.cluster.local:5080")
CLUSTER_NAME = os.getenv('CLUSTER_NAME', "test-cluster")

def get_mdm_information(namespace, pod_name):
    # Example of an MDM resonse
    '''
    spec:
      groups: codeco.com
      versions:
      - name: v1
        served: 'true'
        storage: 'true'
        schema:
          openAPIV3Schema:
            type: object
            properties:
              spec:
                type: object
                properties:
                  pod_name:
                    description: identifier of the node
                    type: string
                    value: acm-swm-app-backend
                  freshness:
                    description: healthiness of the node based on data freshness
                    type: string
                    value: n/a
                  compliance:
                    description: compliance to application requirements
                    type: string
                    value: '0.7096663'
                  portability:
                    description: portability level for a specific application
                    type: string
                    value: '1.0'
      scope: Namespaced
      names:
        plural: mdm-mons
        singular: mdm-mon
        kind: MDM
        shortNames:
        - mdm-m
    '''
    # Contacting MDM Information
    print("[MDM] MDM URL:", MDM_URL)
    print("[MDM] CLUSTER CONTEXT:", CLUSTER_NAME)
    response = requests.get(MDM_URL+"/mdm/api/v1/pdlc?cluster="+CLUSTER_NAME+"&namespace="+namespace+"&pod="+pod_name)
    text = response.text
    print("------- RAW MDM Text -------")
    print(text)

    res = {
        "cluster" : CLUSTER_NAME,
        "namespace" : namespace,
        "pod_name" : pod_name,
        "freshness" : "N/A",
        "compliance" : "N/A",
        "portability": "N/A"
    }
    
    yaml_data = yaml.safe_load(text)    # Text in YAML format
    properties = yaml_data["spec"]["versions"][0]["schema"]["openAPIV3Schema"]["properties"]["spec"]["properties"]

    # Get the needed values
    res["freshness"] = properties["freshness"]["value"]
    res["compliance"] = properties["compliance"]["value"]
    res["portability"] = properties["portability"]["value"]
    
    print("------- MDM Collected by PDLC -------")
    print(res)
    return res

""" ACM Caller """

# CRD Definitions
ACM_GROUP = os.getenv("ACM_GROUP", "codeco.he-codeco.eu")
ACM_VERSION = os.getenv("ACM_VERSION", "v1alpha1")
ACM_NAMESPACE = os.getenv("ACM_NAMESPACE", "he-codeco-acm")
ACM_PLURAL = os.getenv("ACM_PLURAL", "codecoapps")

def get_acm_information(cluster_sample):
    # Kubernetes variables
    config.load_incluster_config()
    crd_client = client.CustomObjectsApi()

    while True:
        try:
            # Fetch the CRD instance
            acm_instances = crd_client.list_namespaced_custom_object(
                group=ACM_GROUP,
                version=ACM_VERSION,
                namespace=ACM_NAMESPACE,
                plural=ACM_PLURAL
            )
            acm_json = acm_instances["items"][0]
            break
        except:
            print("BUG ON ACM:", acm_instances)
            time.sleep(1)
            continue

    # print("ACM Instance", acm_instances["items"][0])
    acm_json = acm_instances["items"][0]
    print("Printing ACM JSON:", acm_json)

    acm_spec = acm_json["spec"]
    acm_status = acm_json["status"]

    app_name = acm_spec["appName"]
    if "namespace" not in acm_json["metadata"]:
        app_namespace = "default"
    else:
        app_namespace = acm_json["metadata"]["namespace"]
    app_cluster = CLUSTER_NAME

    # Find MAX CPU and MEM in Resources
    max_cpu = -1
    max_mem = -1
    for service in acm_spec["codecoapp-msspec"]:
        if type(service) is not dict:
            continue
        containers = service["podspec"]["containers"]
        for container in containers:
            if "resources" not in container:
                continue
            resources = container["resources"]
            # Check for MAX values
            if int(resources["limits"]["cpu"]) > max_cpu:
                max_cpu = int(resources["limits"]["cpu"])
            if int(resources["limits"]["memory"][:-2]) > max_mem:
                max_mem = int(resources["limits"]["memory"][:-2])

    # Finalize MAX Values with defaults
    if max_cpu == -1:
        max_cpu == 4
    if max_mem == -1:
        max_mem == 4
    max_cpu = str(max_cpu)
    max_mem = str(max_mem) + "Gi"

    # Run through pods
    for pod in acm_status["appMetrics"]["serviceMetrics"]:
        pod_obj = {}
        pod_obj["appName"] = app_name
        pod_obj["pod_name"] = app_name + "-" + pod["serviceName"]
        pod_obj["pod_cpu"] = pod["avgServiceCpu"]
        pod_obj["pod_mem"] = str(format(float(pod["avgServiceMemory"]) / 1024.0**3,".6f")) # ACM Provides this in bytes / RL needs it in Gi
        pod_obj["pod_failure"] = pod["avgServiceFailure"] if "avgServiceFailure" in pod else "0"
        pod_obj["pod_energy"] = pod["avgServiceEnergy"] if "avgServiceEnergy" in pod else "0"
        pod_obj["pod_sec"] = pod["avgServiceSecurity"] if "avgServiceSecurity" in pod else "0"
        pod_obj["service_name"] = pod["serviceName"]
        pod_obj["max_pod_cpu"] = max_cpu
        pod_obj["max_pod_mem"] = max_mem

        # Get MDM Information
        mdm_obj = get_mdm_information(app_namespace, pod_obj["pod_name"])

        pod_obj["cluster"] = app_cluster
        pod_obj["namespace"] = app_namespace
        pod_obj["freshness"] = mdm_obj["freshness"]
        pod_obj["compliance"] = mdm_obj["compliance"]
        pod_obj["portability"] = mdm_obj["portability"]
        
        if pod["nodeName"] not in cluster_sample:
            # For testing perpuses
            list_of_nodes = []
            for node_name in cluster_sample:
                if cluster_sample[node_name]["type"] == "C":
                    list_of_nodes.append(node_name)
            random_node_name = random.choice(list_of_nodes)
            cluster_sample[random_node_name]["pods"].append(pod_obj)
        else:
            cluster_sample[pod["nodeName"]]["pods"].append(pod_obj)
    
    # Add Testing Node Information (testing perpuses)
    if acm_status["nodeMetrics"][0]["nodeName"] not in cluster_sample:
        # For testing perpuses
        for node_name in cluster_sample:
            if cluster_sample[node_name]["type"] == "C":
                cluster_sample[node_name]["node_cpu"] = acm_status["nodeMetrics"][0]["avgNodeCpu"]
                cluster_sample[node_name]["node_mem"] = str(format(float(acm_status["nodeMetrics"][0]["avgNodeMemory"]) / 1024.0**3,".6f")) # ACM Provides this in bytes / RL needs it in Gi
                cluster_sample[node_name]["node_failure"] = acm_status["nodeMetrics"][0]["avgNodeFailure"] if "avgNodeFailure" in acm_status["nodeMetrics"][0] else "0"
                cluster_sample[node_name]["node_energy"] = acm_status["nodeMetrics"][0]["avgNodeEnergy"] if "avgNodeEnergy" in acm_status["nodeMetrics"][0] else "0"
                cluster_sample[node_name]["node_sec"] = acm_status["nodeMetrics"][0]["avgNodeSecurity"] if "avgNodeSecurity" in acm_status["nodeMetrics"][0] else "0"
        return cluster_sample   # Return the cluster sample with dummy node information

    # Add Node Information
    for node in acm_status["nodeMetrics"]:
        cluster_sample[node["nodeName"]]["node_cpu"] = node["avgNodeCpu"]
        cluster_sample[node["nodeName"]]["node_mem"] = str(format(float(node["avgNodeMemory"]) / 1024.0**3,".6f")) # ACM Provides this in bytes / RL needs it in Gi
        cluster_sample[node["nodeName"]]["node_failure"] = node["avgNodeFailure"] if "avgNodeFailure" in node else "0"
        cluster_sample[node["nodeName"]]["node_energy"] = node["avgNodeEnergy"] if "avgNodeEnergy" in node else "0"
        cluster_sample[node["nodeName"]]["node_sec"] = node["avgNodeSecurity"] if "avgNodeSecurity" in node else "0"

    # Add Information if nodes are not listed in ACM CRD
    for node_name in cluster_sample:
        if "node_cpu" in cluster_sample[node_name]:
            continue
        if cluster_sample[node_name]["type"] != "C":
            continue
        
        # Add the Information
        cluster_sample[node_name]["node_cpu"] = acm_status["nodeMetrics"][0]["avgNodeCpu"]
        cluster_sample[node_name]["node_mem"] = str(format(float(acm_status["nodeMetrics"][0]["avgNodeMemory"]) / 1024.0**3,".6f")) # ACM Provides this in bytes / RL needs it in Gi
        cluster_sample[node_name]["node_failure"] = acm_status["nodeMetrics"][0]["avgNodeFailure"] if "avgNodeFailure" in acm_status["nodeMetrics"][0] else "0"
        cluster_sample[node_name]["node_energy"] = acm_status["nodeMetrics"][0]["avgNodeEnergy"] if "avgNodeEnergy" in acm_status["nodeMetrics"][0] else "0"
        cluster_sample[node_name]["node_sec"] = acm_status["nodeMetrics"][0]["avgNodeSecurity"] if "avgNodeSecurity" in acm_status["nodeMetrics"][0] else "0"

    return cluster_sample

""" NetMA Caller """

# CRD Definitions
NETMA_GROUP = os.getenv("NETMA_GROUP", "codeco.com")
NETMA_VERSION = os.getenv("NETMA_VERSION", "v1")
NETMA_NAMESPACE = os.getenv("NETMA_NAMESPACE", "he-codeco-netma")
NETMA_PLURAL = os.getenv("NETMA_PLURAL", "netma-topologies")

def get_netma_information():
    # Kubernetes variables
    config.load_incluster_config()
    crd_client = client.CustomObjectsApi()

    # Fetch the CRD instance
    netma_instances = crd_client.list_namespaced_custom_object(
        group=NETMA_GROUP,
        version=NETMA_VERSION,
        namespace=NETMA_NAMESPACE,
        plural=NETMA_PLURAL
    )

    netma_json = netma_instances["items"][0]

    netma_u_json_spec = netma_json["underlay-topology"]
    netma_o_json_spec = netma_json["overlay-topology"]

    cluster_sample = {}

    # Node Information
    for node in netma_u_json_spec["nodes"]:
        # A Dictionary for the information of a node
        current_node_information = {}
        
        current_node_information["node_name"] = node["name"]
        current_node_information["type"]      = "C" if node["type"] == "COMPUTE" else "N"
        current_node_information["links"] = []
        current_node_information["pods"] = []
        
        if current_node_information["type"] == "C":
            current_node_information["ebw"]  = node["capabilities"]["uNodeBandWidth"]
            # current_node_information["node_throughput"]  = node["capabilities"]["uNodeThroughput"]
            current_node_information["node_degree"]  = node["capabilities"]["uNodeDegree"]
        
        if current_node_information["type"] == "N":
            current_node_information["node_net_failure"] = node["capabilities"]["uNodeNetFailure"]
        
        # Add node in cluster_sample
        cluster_sample[node["name"]] = current_node_information

    for node in netma_o_json_spec["nodes"]:
        cluster_sample[node["name"]]["node_throughput"] = node["capabilities"]["oNodeThroughput"]

    # Link Information
    for link in netma_u_json_spec["links"]:
        link_obj = {}
        link_obj["type"] = cluster_sample[link["target"]]["type"]
        link_obj["node_name"] = link["target"]
        link_obj["link_id"] = link["name"]
        link_obj["link_type"] = "P" # Physical Links
        
        # Network Extra Information
        if cluster_sample[link["target"]]["type"] == "N" and cluster_sample[link["source"]]["type"] == "N":
            link_obj["link_failure"] = link["capabilities"]["uLinkFailure"]
            link_obj["link_energy"] = link["capabilities"]["uLinkEnergy"]
        
        # Append the Network links
        cluster_sample[link["source"]]["links"].append(link_obj)

    for link in netma_o_json_spec["links"]:
        link_obj = {}
        link_obj["type"] = cluster_sample[link["target"]]["type"]
        link_obj["node_name"] = link["target"]
        link_obj["link_id"] = link["name"]
        link_obj["link_type"] = "L" # Logical Links
        link_obj["ibw"] = link["capabilities"]["oBandWidthBits"]
        link_obj["latency"] = link["capabilities"]["oLatencyNanos"]
        
        # Append the Network links
        cluster_sample[link["source"]]["links"].append(link_obj)
        
    # Path Information
    for path in netma_u_json_spec["paths"]:
        for link in cluster_sample[path["source"]]["links"]:
            if link["node_name"] == path["target"]:
                link["packet_loss"] = path["capabilities"]["uPacketLoss"]
                link["path_failure"] = path["capabilities"]["uPathFailure"]
                link["path_length"] = path["capabilities"]["uPathLength"]

    # Computing IBW for Nodes
    for node_name in cluster_sample:
        if cluster_sample[node_name]["type"] == "N":
            continue
        node = cluster_sample[node_name]
        sum = 0
        for link in node["links"]:
            if link["type"] == "C":
                sum += int(float(link["ibw"]))
        cluster_sample[node_name]["ibw"] = str(sum) + "G"

    return cluster_sample

TOPOLOGY_FILE = os.getenv("TOPOLOGY_FILE", "data/topology/config.json")

""" TOPOLOGY CREATOR """
def create_topology_file(cluster_sample):
    """ Example of the Topology file found in /data/topology/config.json"""
    """
    {
      "node_names": [
        "sonem-control-plane",
        "sonem-worker"
      ],
      "connections": [
        [1, 1],
        [1, 1]
      ]
    }
    """

    # Create the node_names
    node_names = []
    for node_name in cluster_sample:
        if node_name == "timestamp":
            continue

        if cluster_sample[node_name]["type"] == "N":
            continue

        node_names.append(node_name)   

    # Create the connections
    connections = []
    for node_name in node_names:
        # Get the index of the node in the node_names
        current_node_index = node_names.index(node_name)

        # Create an empty list for the connections of the node, having zeros for the connections
        node_connections = [0] * len(node_names)

        # Get the information of the node
        node = cluster_sample[node_name]
        for link in node["links"]:
            if link["type"] == "N":
                continue

            # Get the index of the target node
            target_node_index = node_names.index(link["node_name"])

            # Update the connection
            node_connections[target_node_index] = 1

        # Update the connection of the node
        node_connections[current_node_index] = 1

        # Append the connection to the connections list
        connections.append(node_connections)

    # Create the topology file
    topology_file = {
        "node_names": node_names,
        "connections": connections
    }

    # Create the directory and the file if it does not exist
    os.makedirs(os.path.dirname(TOPOLOGY_FILE), exist_ok=True)

    # Write the topology file
    with open(TOPOLOGY_FILE, "w") as f:
        # f.write(str(topology_file))
        json.dump(topology_file, f, indent=4)
    return

# Check That a CODECO Application is deployed
def codeco_application_deployed():
    # Kubernetes variables
    config.load_incluster_config()
    crd_client = client.CustomObjectsApi()

    # Fetch the CRD instance
    try:
        acm_instances = crd_client.list_namespaced_custom_object(
            group=ACM_GROUP,
            version=ACM_VERSION,
            namespace=ACM_NAMESPACE,
            plural=ACM_PLURAL
        )
        return True
    except:
        return False

def get_sample():
    while True:
        try:
            cluster_sample = get_netma_information()        # Get the initial information of the sample
            break
        except Exception as err:
            print(f"Unexpected NETMA Error {err=}, {type(err)=}")
            time.sleep(1)
            continue
    
    cluster_sample = get_acm_information(cluster_sample)          # Get the ACM information

    # Create the topology file in the Shared Volume
    create_topology_file(cluster_sample)            # Create the topology file

    # Get the datetime
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    cluster_sample["timestamp"] = timestamp       # Add the timestamp to the sample
    return cluster_sample
