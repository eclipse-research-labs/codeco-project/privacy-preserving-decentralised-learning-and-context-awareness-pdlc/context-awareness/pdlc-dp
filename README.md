<!--
  ~ Copyright (c) 2024 University of Piraeus Research Centre
  ~ 
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~ 
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~ 
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~ 
  ~ SPDX-License-Identifier: Apache-2.0
  ~ 
  ~ Contributors:
  ~     Efterpi Paraskevoulakou (UPRC) - Co-Author
  ~     Panagiotis Karamolegkos (UPRC) - Co-Author
-->

# Description
The code in this repository is the Data preprocessing (DP) subcomponent of CODECO's PDLC Component. PDLC-DP's major objective is to deliver high-quality, interpretable data, continuously collected from ACM, MDM, and NetMA CRDs, using techniques like cleaning, integration, transformation, and normalization. This data is then provided to other PDLC subcomponents, PDLC-CA and PDLC-DL, for further processing. The PDLC-DP architecture includes two main modules:

- <u>**a Gatherer**</u> responsible for collecting the data from other PDLC components and,
- <u>**a Pre-processing module**</u>  responsible for transforming the collected data. 

    ### 1. Selected Technologies
    The PDLC-DP sub-component has been designed and developed entirely by using the **Python programming language versions 3.10+ & 3.12+**. The respective packages that were used during the component’s development are listed below along with their relevant description: 

 

    - **Kubernetes**: a Python client for Kubernetes 

    - **NumPy** (Numerical Python): an open source, widely used Python library that provides computation functionalities and fast operations on multidimensional array objects, mostly used in the model’s data preprocessing code. 

    - **Pandas** (Python Data Analysis): an open-source Python library that uses DataFrame and Series data structures for efficient and flexible data analysis and manipulation, mostly used in the model’s data preprocessing code 

    - **Requests**: an open-source library that allows sending HTTP requests 

    ### 2. Python requirements:
    The below list presents the Python packages that have been used to implement the PDLC-DP component along with their respective versions:

    - cachetools==5.3.3 
    - certifi==2024.6.2 
    - charset-normalizer==3.3.2 
    - google-auth==2.29.0 
    - idna==3.7 
    - kubernetes==29.0.0 
    - numpy==1.26.4 
    - oauthlib==3.2.2 
    - pandas==2.2.2 
    - pyasn1==0.6.0 
    - pyasn1_modules==0.4.0 
    - python-dateutil==2.9.0.post0 
    - pytz==2024.1 
    - PyYAML==6.0.1 
    - requests==2.32.3 
    - requests-oauthlib==2.0.0 
    - rsa==4.9 
    - six==1.16.0 
    - tzdata==2024.1 
    - urllib3==2.2.1 
    - websocket-client==1.8.0 
    - wheel==0.43.0

 ## Code structure and respective description
 | File  | Function  | 
|-------------|-------------|
| [apply-component.sh](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/blob/main/apply-component.sh?ref_type=heads)| Installs PDLC-DP in the cluster  | 
| [delete-component.sh](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/blob/main/delete-component.sh?ref_type=heads)  | Removes PDLC-DP from the cluster  | 
| [src/k8s_calling/sample.py](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/blob/main/src/k8s_calling/sample.py?ref_type=heads) | This script is used to generate fake CRs from CRDs in order to be tested from the PDLC-DP. Additionally, grabs the sample topology from the NetMA CR and stores it in the defined persistent volume used by all the PDLC subcomponents| 
| [src/models ](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/tree/main/src/models?ref_type=heads) | Includes all the pre-trained models for both the GNNs and RL. For the RL models, examples are provided illustrating how the records are represented in the CRD utilized by SWM. These models are copied directly to the defined persistent volume used by the PDLC subcomponents. | 
| [src/gatherer.py  ](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/blob/main/src/gatherer.py?ref_type=heads) | The gathering module. It is responsible for gathering the CRs from CRDs. Once the dataflow is initialized the gatherer creates queues for providing the data to the preprocessing module using threads, each one for each PDLC subcomponent.| 
 [src/pp_module/ca.py   ](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/blob/main/src/pp_module/ca.py?ref_type=heads) | This script is responsible for executing all the preprocessing operations concerning the PDLC-CA subcomponent. It is located in the folder named `pp_module`, which is part of the preprocessing module. | 
 [src/pp_module/gnn.py   ](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/blob/main/src/pp_module/gnn.py?ref_type=heads) | This script is responsible for executing all the preprocessing operations required for the GNNs as part of the PDLC-DL subcomponent. It is located in the folder named `pp_module`, which is part of the preprocessing module.  | 
 [src/pp_module/rl.py](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/blob/main/src/pp_module/rl.py?ref_type=heads) | This script is responsible for executing all the preprocessing operations required for the RL as part of the PDLC-DL subcomponent. It is located in the folder named `pp_module`, which is part of the preprocessing module.| 
  [src/pp_module/features_dict.py ](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/blob/main/src/pp_module/features_dict.py?ref_type=heads) | This script serves as a guide for correcting the labelling of cross-layer metrics and functions as a support tool. It is located in the folder named `pp_module`, which is part of the preprocessing module. | 
  [src/pp_module/utilities.py ](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/blob/main/src/pp_module/utilities.py?ref_type=heads) | This script, located in the pp_module folder as part of the preprocessing module, provides support functions for the key functions (CA, RL, GNNS). It is located in the folder named `pp_module`, which is part of the preprocessing module. |
  [src/data_examples/](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/privacy-preserving-decentralised-learning-and-context-awareness-pdlc/pdlc-dp/-/tree/main/src/data_examples?ref_type=heads) | This directory includes examples of the PDLC-DP input data and examples representing the final output .csv files that PDLC-DP provides to the other PDLC subcomponents (CA, GNNs, RL) - in different sub-directories |  



After installing the PDLC-DP, it collects Custom Resources (CR) and sends it for preprocessing. Within the pod used for the scripts, files are generated in the `/data` directory. These files contain information to be utilized by the remaining PDLC subcomponents.


# Prerequisites


- A Kubernetes Cluster
- Synthetic Data Generator installed (mandatory with the mock CRDs if PDLC is running standalone) [1] <span style="color:red">(To deploy PDLC in **CODECO**, the Synthetic Data Generator must not be deployed)</span>
- Administrator privileges on the cluster, with access to the `default` namespace and **Service Account**.

---


# Deployment
Before deploying the component in a K8s Cluster, you need to write the name of the Cluster in the `CLUSTER_NAME` Enviromental Variable of the following CRD:
```
pdlc-dp/src/pdlc-dp-deployment.yaml
```

Run the following command. This command will install all the needed YAML files in the cluster to deploy the subcomponent.
```
./apply-component.sh
```
# Remove from Cluster
To remove the YAML files from the cluster you can run the following command:
```
./delete-component.sh
```


# References
1. [Synthetic Data Generator](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/experimentation-framework-and-demonstrations/data-generators-and-datasets/synthetic-data-generator)

# Authors
- Panagiotis Karamolegkos (UPRC)
- Pepi Paraskevoulakou (UPRC)